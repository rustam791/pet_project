const util = require('util');
const exec = util.promisify(require('child_process').exec);

(async () => {
    try {
        const out = await exec(`netstat -ltnp | grep ${process.argv[2] ? process.argv[2] : 4444}`);    
        if(out.stdout === '') return true
        const { stdout } = await exec(`kill -9 ${process.argv[2] ? out.stdout.match(/[0-9]*\/ng serve/g)[0].replace(/\/ng serve/, '') : out.stdout.match(/[0-9]*\/node/g)[0].replace(/\/node/, '')}`)
    } catch (error) {
        console.error(error);
    }
})()
