import {Injectable} from '@angular/core';
import { Product } from 'src/app/interface/product';
import { Observable, of } from 'rxjs';

@Injectable()
export class DataProduct {
    constructor() {console.log('1111')}
    private products: Product[] = [
        {id: 1, name: "Product 1", category:"Category 1", price:'100', image:"https://via.placeholder.com/500/771796", description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error eligendi quasi inventore omnis neque quos ipsam, blanditiis ipsa eaque veniam dolor laboriosam debitis commodi tempore harum molestiae. Autem, est dolorem.'},
        {id: 2, name: "Product 2", category:"Category 2", price:'102', image:"https://via.placeholder.com/500/771796", description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error eligendi quasi inventore omnis neque quos ipsam, blanditiis ipsa eaque veniam dolor laboriosam debitis commodi tempore harum molestiae. Autem, est dolorem2.'},
        {id: 3, name: "Product 3", category:"Category 3", price:'103', image:"https://via.placeholder.com/500/771796", description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error eligendi quasi inventore omnis neque quos ipsam, blanditiis ipsa eaque veniam dolor laboriosam debitis commodi tempore harum molestiae. Autem, est dolorem3.'},
        {id: 4, name: "Product 4", category:"Category 4", price:'104', image:"https://via.placeholder.com/500/771796", description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error eligendi quasi inventore omnis neque quos ipsam, blanditiis ipsa eaque veniam dolor laboriosam debitis commodi tempore harum molestiae. Autem, est dolorem4.'},
        {id: 5, name: "Product 5", category:"Category 5", price:'105', image:"https://via.placeholder.com/500/771796", description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error eligendi quasi inventore omnis neque quos ipsam, blanditiis ipsa eaque veniam dolor laboriosam debitis commodi tempore harum molestiae. Autem, est dolorem5.'},
        {id: 6, name: "Product 6", category:"Category 6", price:'106', image:"https://via.placeholder.com/500/771796", description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error eligendi quasi inventore omnis neque quos ipsam, blanditiis ipsa eaque veniam dolor laboriosam debitis commodi tempore harum molestiae. Autem, est dolorem6.'},
        {id: 7, name: "Product 7", category:"Category 7", price:'107', image:"https://via.placeholder.com/500/771796", description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error eligendi quasi inventore omnis neque quos ipsam, blanditiis ipsa eaque veniam dolor laboriosam debitis commodi tempore harum molestiae. Autem, est dolorem7.'},
        {id: 8, name: "Product 8", category:"Category 8", price:'108', image:"https://via.placeholder.com/500/771796", description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error eligendi quasi inventore omnis neque quos ipsam, blanditiis ipsa eaque veniam dolor laboriosam debitis commodi tempore harum molestiae. Autem, est dolorem8.'},
        {id: 9, name: "Product 9", category:"Category 9", price:'109', image:"https://via.placeholder.com/500/771796", description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error eligendi quasi inventore omnis neque quos ipsam, blanditiis ipsa eaque veniam dolor laboriosam debitis commodi tempore harum molestiae. Autem, est dolorem9.'},
        {id: 10, name: "Product 10", category:"Category 10", price:'1010', image:"https://via.placeholder.com/500/771796", description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error eligendi quasi inventore omnis neque quos ipsam, blanditiis ipsa eaque veniam dolor laboriosam debitis commodi tempore harum molestiae. Autem, est dolorem10.'},
        {id: 11, name: "Product 11", category:"Category 11", price:'1011', image:"https://via.placeholder.com/500/771796", description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error eligendi quasi inventore omnis neque quos ipsam, blanditiis ipsa eaque veniam dolor laboriosam debitis commodi tempore harum molestiae. Autem, est dolorem11.'},
        {id: 12, name: "Product 12", category:"Category 12", price:'1012', image:"https://via.placeholder.com/500/771796", description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error eligendi quasi inventore omnis neque quos ipsam, blanditiis ipsa eaque veniam dolor laboriosam debitis commodi tempore harum molestiae. Autem, est dolorem12.'},
        {id: 13, name: "Product 13", category:"Category 13", price:'1013', image:"https://via.placeholder.com/500/771796", description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error eligendi quasi inventore omnis neque quos ipsam, blanditiis ipsa eaque veniam dolor laboriosam debitis commodi tempore harum molestiae. Autem, est dolorem13.'}
    ]
    
    getProduct(): Observable<Product[]> {
        return of(this.products);
    }
}
