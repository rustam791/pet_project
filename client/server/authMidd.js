"use strict";
exports.__esModule = true;
// const JWT = require('jsonwebtoken');
var fs_1 = require("fs");
var jsonwebtoken_1 = require("jsonwebtoken");
var APP_SECRET = 'test_app_secter';
var USERNAME = 'app';
var PASSWORD = 'app';
/**
 * @param  {any} req
 * @param  {any} res
 * @param  {any} next
 * @returns void
 * test middleware() (example 4 book);
 */
module.exports = function (req, res, next) {
    try {
        if (req.method === 'OPTIONS') {
            res.json({
                statusCode: 200
            });
            return;
        }
        if (req.url === '/order' && req.method === 'POST' && req.body) { // only test one connect
            fs_1.readFile('../phatnom_data/data_client/products.json', function (err, data) {
                if (err) {
                    res.json({
                        success: false,
                        message: err,
                        statusCode: 500
                    });
                }
                var DATA = JSON.parse(data.toString());
                DATA.order.push(req.body);
                fs_1.writeFile('../phatnom_data/data_client/products.json', JSON.stringify(DATA), function (err) {
                    if (err) {
                        res.json({
                            success: false,
                            message: err,
                            statusCode: 500
                        });
                    }
                    res.json({
                        success: true,
                        message: 'created',
                        statusCode: 201
                    });
                });
            });
        }
        if (req.url === '/login' && req.method === 'POST') {
            console.log(req.body);
            if (req.body !== null && req.body.name === USERNAME && req.body.password === PASSWORD) {
                res.json({
                    success: true,
                    token: jsonwebtoken_1.sign({ data: USERNAME, expiresIn: 60 * 60 * 1000 }, APP_SECRET) // time - millisecond before js Data getTime returned timestamp(millisecond)
                });
            }
            else {
                res.json({ success: false });
            }
        }
        else if ((req.url.startsWith('/products') && req.method !== 'GET') || (req.url.startsWith('/orders') && req.method !== 'POST')) {
            var TOKEN = req.headers['authorization'];
            console.log(TOKEN);
            if (TOKEN !== null && TOKEN.startsWith('Bearer<')) {
                TOKEN = TOKEN.substring(7, TOKEN.length - 1);
                try {
                    jsonwebtoken_1.verify(TOKEN, APP_SECRET);
                    next();
                    return;
                }
                catch (error) { }
            }
            res.statusCode = 401;
            res.end();
            return;
        }
        next();
    }
    catch (err) {
        console.log(err);
        res.json({
            success: false,
            message: err,
            statusCode: 500
        });
        next();
    }
};
