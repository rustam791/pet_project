// const JWT = require('jsonwebtoken');
import { readFile, readFileSync, writeFile, writeFileSync } from 'fs';
import { sign, verify } from 'jsonwebtoken';

const APP_SECRET = 'test_app_secter';
const USERNAME = 'app';
const PASSWORD = 'app';
/**
 * @param  {any} req
 * @param  {any} res
 * @param  {any} next
 * @returns void
 * test middleware() (example 4 book);
 */
module.exports = function (req: any, res: any, next: any): void {
    try {
        if(req.method === 'OPTIONS') {
            res.json({
                statusCode:200
            });
            return;
        }
        if (req.url === '/order' && req.method === 'POST' && req.body) {// only test one connect
            readFile('../phatnom_data/data_client/products.json', (err, data) => {
                if (err) {
                    res.json({
                        success: false,
                        message: err,
                        statusCode: 500
                    });
                }
                const DATA = JSON.parse(data.toString());
                DATA.order.push(req.body);
                writeFile('../phatnom_data/data_client/products.json', JSON.stringify(DATA), err => {
                    if (err) {
                        res.json({
                            success: false,
                            message: err,
                            statusCode: 500
                        });
                    }
                    res.json({
                        success: true,
                        message: 'created',
                        statusCode: 201
                    });
                });


            })

        }

        if (req.url === '/login' && req.method === 'POST') {
            console.log(req.body);
            
            if (req.body !== null && req.body.name === USERNAME && req.body.password === PASSWORD) {
                res.json({
                    success: true,  // or only token with test required token
                    token: sign({ data: USERNAME, expiresIn: 60 * 60 * 1000 }, APP_SECRET) // time - millisecond before js Data getTime returned timestamp(millisecond)
                });
            } else {
                res.json({ success: false });
            }
        } else if ((req.url.startsWith('/products') && req.method !== 'GET') || (req.url.startsWith('/orders') && req.method !== 'POST')) {
            let TOKEN: string = req.headers['authorization'];
            console.log(TOKEN);
            if (TOKEN !== null && TOKEN.startsWith('Bearer<')) {
                TOKEN = TOKEN.substring(7, TOKEN.length - 1);
                try {
                    verify(TOKEN, APP_SECRET);
                    next();
                    return;
                } catch (error) { }
            }
            res.statusCode = 401;
            res.end();
            return;
        }
        next();
    } catch (err) {
        console.log(err);
        res.json({
            success: false,
            message: err,
            statusCode: 500
        });
        next();

    }
}


