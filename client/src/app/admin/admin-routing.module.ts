import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminAuthGuard } from '../guards/admin-auth.guard';
import { AuthAdminGuard } from '../guards/auth-admin.guard';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AdminAuthGuard]
  },
  {
    path:'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
    canActivate: [AuthAdminGuard]
  },
  {
    path:'orders',
    loadChildren: () => import('./orders/orders.module').then(m => m.OrdersModule)
    , canActivate: [AdminAuthGuard]
  },
  {
    path:'products',
    loadChildren: () => import('./products/products.module').then(m => m.ProductsModule),
    canActivate: [AdminAuthGuard]
  },
  {
    path: '**',
    redirectTo: 'auth'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
