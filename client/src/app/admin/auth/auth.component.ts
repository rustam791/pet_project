import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { API_URL } from 'src/app/services/constants';
import { map } from 'rxjs/operators';

import { Subscription } from 'rxjs';
import { ResponseToken } from 'src/app/interface/response-token';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, OnDestroy {

  formLogin: FormGroup = new FormGroup({
    name: new FormControl('', {validators: Validators.required, updateOn: 'blur'} ),
    password: new FormControl('', {validators: Validators.required, updateOn: 'blur'} ),
  });

  arraySubcriber: Array<Subscription> = [];

  constructor(private http: HttpClient) { }
  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.arraySubcriber.map(subscriber => subscriber.unsubscribe());
  }

  login(): void{
    if(this.formLogin.invalid) return;
    this.arraySubcriber.push(this.http.post(`${API_URL}login`, this.formLogin.value)
      .pipe(map((res: ResponseToken) => 
        res.success ? res.token : new Error('Ошибка!!!')
      ))
      .subscribe(resp => localStorage.setItem('token', <string>resp)
      , err => console.error(err)));
  }

}
