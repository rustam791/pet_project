import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrderResolver } from 'src/app/resolvers/order.resolver';
import { OrdersComponent } from './orders.component';

const routes: Routes = [
  {
    path:'',
    component: OrdersComponent,
    resolve: {
      order: OrderResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
