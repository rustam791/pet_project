import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Order } from 'src/app/interface/order';
import { STATE_ORDERS } from 'src/app/services/constants';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit, OnDestroy {


  private $stramsArray: Subscription[] = [];
  private orders!: FormGroup[];
  private formBuiolder: FormBuilder = new FormBuilder();

  constructor(private apiOrder: OrderService, private api: ActivatedRoute) {
    this.$stramsArray.push(this.api.data.subscribe(order =>
      this.orderListGroup = order.order.map((orderItem: Order) => this.buildFrom(orderItem))

    ));
    console.log(this.orderListGroup[0]);
  }




  ngOnInit(): void {
    console.log(this.orderListGroup);
  }
  ngOnDestroy(): void {
    this.$stramsArray.map($stream => $stream.unsubscribe());
  }

  buildFrom(order: Order): FormGroup {
    const FORM = {};
    Object.keys(order).map((key: string) => {
      if (typeof order[key] === 'object') return;
      Object.defineProperty(FORM, key, {
        value: new FormControl(order[key], Validators.required),
        enumerable: true
      })
    })
    return new FormGroup(FORM);
  }

  /**
   * get list orders
   * @returns void
   */
  // reqOrder(): void {
  //   this.$stramsArray.push(
  //     this.apiOrder.getOrders()
  //       .subscribe((order: Order[]) => this.orderList = order)
  //   );
  // }



  get orderListGroup(): FormGroup[] {
    return this.orders;
  }

  set orderListGroup(orders: FormGroup[]) {
    this.orders = orders;
  }

  get states(): string[] {
    return STATE_ORDERS;
  }

  save(form: FormGroup) {
    const data = form.value;
    this.apiOrder.updateOrder(data).subscribe(data => {
      console.log(data);
      form.reset();
      form.setValue(data);
    });
  }

  changeForm(form: FormGroup) {
    console.log(form);
  }


}
