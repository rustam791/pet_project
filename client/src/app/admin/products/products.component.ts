import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Product } from 'src/app/interface/product';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {


  private $stramsArray: Subscription[] = [];
  public product!: FormGroup[];
  private formBuiolder: FormBuilder = new FormBuilder();

  constructor(private apiProduct: ProductsService, private api: ActivatedRoute) {
    this.$stramsArray.push(this.api.data.subscribe(product =>
      this.productListGroup = product.product.map((productItem: Product) => this.buildFrom(productItem))
    ));

  }




  ngOnInit(): void {
  }
  ngOnDestroy(): void {
    this.$stramsArray.map($stream => $stream.unsubscribe());
  }

  buildFrom(product: Product): FormGroup {
    const FORM = {};
    Object.keys(product).map((key: string) => {
      if (typeof product[key] === 'object') return;
      Object.defineProperty(FORM, key, {
        value: new FormControl(product[key], Validators.required),
        enumerable: true
      })
    })
    return new FormGroup(FORM);
  }

  /**
   * get list orders
   * @returns void
   */
  // reqOrder(): void {
  //   this.$stramsArray.push(
  //     this.apiOrder.getOrders()
  //       .subscribe((order: Order[]) => this.orderList = order)
  //   );
  // }



  get productListGroup(): FormGroup[] {
    return this.product;
  }

  set productListGroup(products: FormGroup[]) {
    this.product = products;
  }

  // get states(): string[] {
  //   return STATE_ORDERS;
  // }

  save(form: FormGroup) {
    const data = form.value;
    this.apiProduct.updateProduct(data).subscribe(data => {
      form.reset();
      form.setValue(data);
    });
  }

  changeForm(form: FormGroup) {
    console.log(form);
  }
}
