import { ApplicationRef, ChangeDetectionStrategy, ChangeDetectorRef, Component, ComponentFactoryResolver, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { BehaviorSubject, concat, interval, Observable, Subject } from 'rxjs';
import { concatMap, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { HeaderComponent } from './header/header.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
  ,changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = 'pet-proj';
  

  hash = '1';


  arr = [
    {s: 1, a:2},
    {s: 1, a:2},
    {s: 1, a:2},
    {s: 1, a:2}
  ]


  inc = 1;
  // s = new BehaviorSubject(1).pipe(concatMap(val => {
  //   return interval(1000);
  // }))
  @ViewChild("alertContainer", { read: ViewContainerRef }) containerDynamic = null;

  constructor(
    private ref: ApplicationRef,
    private componentFactory: ComponentFactoryResolver,
    private cdr: ChangeDetectorRef,
    private container: ViewContainerRef,
    private template: TemplateRef<any>
  ) {
    // this.container.createEmbeddedView(this.template);
    this.container.clear()

    ;(<any>window).ref = this.ref;
    this.container.createComponent(this.componentFactory.resolveComponentFactory(HeaderComponent));
  }

  cl() {

    this.cdr.detectChanges()
    
    console.log(this.hash);

  }

  ngOnChanges() {

    console.log('ngOnChanges');
  }
  ngDoCheck() {
    // this.arr = this.arr

    
    console.log('ngDoCheck');
  }
}
