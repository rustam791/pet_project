import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CartDetailRoutingModule } from './cart-detail-routing.module';
import { CartDetailComponent } from './cart-detail.component';
import { CartService } from '../services/cart.service';


@NgModule({
  declarations: [
    CartDetailComponent
  ],
  imports: [
    CommonModule,
    CartDetailRoutingModule
  ],

})
export class CartDetailModule { }
