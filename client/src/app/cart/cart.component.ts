import { Component, OnInit } from '@angular/core';
import { Cart } from '../interface/cart';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
  // changeDetection: ChangeDetectionStrategy.Default
})
export class CartComponent implements OnInit {

  constructor(public cart: CartService) { }

  ngOnInit(): void {
  }

}
