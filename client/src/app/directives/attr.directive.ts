import { Attribute, Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appAttr]'
})
export class AttrDirective {

  constructor(private element: ElementRef, @Attribute("at-app") bgClass: string) { }

}
