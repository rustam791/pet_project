import { Directive, EventEmitter, HostBinding, HostListener, Input, OnChanges, Output, SimpleChange } from '@angular/core';

@Directive({
  selector: 'input[appNcgModel]'
})
export class NcgModelDirective implements OnChanges {

  @Input("appNcgModel")
  modelProperty!: string;

  @HostBinding("value")
  fieldValue: string = "";
  
  @Output("ncgModelChange")
  update = new EventEmitter<string>();

  ngOnChanges(changes: { [property: string]: SimpleChange }) { // изменение
    let change = changes["modelProperty"];
    if (change.currentValue != this.fieldValue) {
      this.fieldValue = changes["modelProperty"].currentValue || "";
    }
  }
  
  
  @HostListener("input", ["$event.target.value"])
  updateValue(newValue: string) {
    this.fieldValue = newValue;
    this.update.emit(newValue);
  }
}
