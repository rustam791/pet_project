import { Directive, Input, OnChanges, SimpleChanges, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[paginationOf]'
})
export class PaginationDirective implements OnChanges {

  constructor(
    private container: ViewContainerRef,
    private template: TemplateRef<Object>
    ) { }



  @Input('paginationOf')
  pagination!: number[];

  ngOnChanges(changes: SimpleChanges): void {
    this.container.clear();
    this.pagination.map(i => 
      this.container.createEmbeddedView(this.template, i)
    );
  }
}

class BuildPagin {
  constructor(public $implicit: any) {
  }
}