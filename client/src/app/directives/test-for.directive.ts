import {
  ChangeDetectorRef,
  Directive,
  Input,
  IterableDiffer,
  IterableDiffers,
  TemplateRef,
  ViewContainerRef,
  OnInit,
  DoCheck,
  ViewRef
} from '@angular/core';

@Directive({
  selector: '[appTestForOf]'
})
export class TestForDirective implements OnInit, DoCheck {

  private _diffrence!: IterableDiffer<any>;
  private _item!: any;
  private views: Map<any, TestForContext> = new Map<any, TestForContext>();

  @Input('appTestForOf') arrTempl: any;

  constructor(
    private container: ViewContainerRef,
    private template: TemplateRef<Object>,
    private differs: IterableDiffers
    //, private changeDetector: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this._item = this.arrTempl;
    if (this._item) {
      this._diffrence = this.differs.find(this._item).create(this.trackByFn);
    }
  }

  ngDoCheck(): void {
    const CHANGES = this._diffrence?.diff(this.arrTempl);
    if (CHANGES) {
      CHANGES.forEachAddedItem(addit => {
        const CONTEXT = new TestForContext(addit.item, addit.currentIndex ?? 0, this.arrTempl.length); 
        CONTEXT.view = this.container.createEmbeddedView(this.template, CONTEXT); // add instanse cintext and create obj
        this.views.set(addit.trackById, CONTEXT);
      });
      let remove = false;
      CHANGES.forEachRemovedItem( removeItem => {
        remove = true;
        const CONTEXT  = this.views.get(removeItem.trackById);
        if(CONTEXT != null) {
          this.container.remove(this.container.indexOf(CONTEXT.view));
          this.views.delete(removeItem.trackById);
        }
      });
      if(remove) {
        let index = 0;
        this.views.forEach(context => 
          context.setdata(index++, this.views.size))
      }
    }
  }


  trackByFn(index: number, item: any): number {
    return index;
  }

}





class TestForContext {
  odd!: boolean;
  even!: boolean;
  first!: boolean;
  last!: boolean;
  view!: ViewRef;

  constructor(
    private $implicit: any,
    private index: number,
    private total: number
  ) {
    this.setdata(index, total) 
  }

  setdata(index: number, total: number) {
    this.odd = index % 2 == 1;
    this.even = !this.odd;
    this.first = index == 0;
    this.last = index == total - 1;
  }
}





