import { Directive, ElementRef, Input, OnChanges, SimpleChanges, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appTestIf]'
})
export class TestIfDirective {

  constructor(
    private container: ViewContainerRef,
    private template: TemplateRef<Object>
  ) { }

  /**
   * @param  {boolean} set appTestIf
   */
  @Input() set appTestIf(cond: boolean) {
    if (cond) {
      this.container.createEmbeddedView(this.template);
    } else
      this.container.clear();
  }

}
