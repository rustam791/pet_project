import { TestBed } from '@angular/core/testing';

import { StoreStartGuard } from './store-start.guard';

describe('StoreStartGuard', () => {
  let guard: StoreStartGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(StoreStartGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
