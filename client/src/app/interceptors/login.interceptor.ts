import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class LoginInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const TOKEN = localStorage.getItem('token');
    if (TOKEN) {
      const CLONE_REQ = request.clone({
        setHeaders: {
          'Authorization': `Bearer<${TOKEN}>`
        }
      });
      return next.handle(CLONE_REQ);
    }

    return next.handle(request);
  }
}
