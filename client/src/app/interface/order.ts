export interface Order {
    [key: string]: string | boolean | number;
    id: number;
    name: string;
    address: string;
    city: string;
    state: string;
    zip: string;
    country: string;
    shipped: boolean;
    phone: string;
    cartPrice: number;
    status: string;
}
