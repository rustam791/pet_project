export interface Product {
    [key: string]: string | boolean | number;
    id: number,
    name: string,
    category: string,
    price: string, // generate faker price string
    image: string,
    description: string
}
