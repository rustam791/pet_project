export interface ResponseToken {
    success?: boolean;
    token?: string;
}
