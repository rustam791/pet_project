/*
*пример из книги расширить до универсальной формы.
*/

import { FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';


export class ProductFControl extends FormControl {
    ERR_CONST: { [key: string]: Function } = {
        'required': (label: string) => `Введите ${label}`,
        'minlength': (minlength: string) => `Длинна должна быть боллее ${minlength}`,
        'maxlength': (maxlength: string) => `Длинна должна быть менее ${maxlength}`,
        'pattern': (pattern: string) => `Символы ${pattern} недопустимы`
    }
    label!: string;
    property!: string
    constructor(
        label: string,
        property: string,
        value: string | number | Object,
        validator: ValidatorFn | ValidatorFn[] | null) {
        super(value, validator);
        this.label = label;
        this.property = property;
    }
    getValidateMessage(): string[] {
        const MESSAGES: string[] = [];
        if (this.errors) {
            for (const ERROR in this.errors) {
                MESSAGES.push(this.ERR_CONST[ERROR]())
            }
        }
        return MESSAGES;
    }
}



export class ProductFGroup extends FormGroup {





    constructor() {
        super({
            name: new ProductFControl('Название', 'name', '', Validators.required),
            category: new ProductFControl('Категория', 'category', '', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z ]+$')])),
            price: new ProductFControl('Цена', 'price', '', Validators.compose([Validators.required, Validators.pattern('^[0-9\.]+$')])),
            description: new ProductFControl('Описание', 'description', '', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z ]+$')])),
            image: new ProductFControl('Картинка', 'image', '', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z ]+$')]))
        });
    }

    get controlsList(): ProductFControl[] {
        return Object.keys(this.controls)
            .map(contr => this.controls[contr] as ProductFControl);
    }


    getFormValidationMessages(form: any): string[] {

        const MESSAGES: string[] = [];
        this.controlsList.map(c => c.getValidateMessage().map(m => MESSAGES.push(m)));
        return MESSAGES;
    }




}




export class LimitValidator {   // протестировать
    static Limit(limit: number) {
        return (control: FormControl): { [key: string]: any } | null => {
            let val = Number(control.value);
            if (val != NaN && val > limit) {
                return { "limit": { "limit": limit, "actualValue": val } };
            } else {
                return null;
            }
        }
    }
}