import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Order } from '../interface/order';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {


  orderSent = false;
  submitted = false;
  formBuilder = new FormBuilder();
  formOrder: FormGroup;

  constructor(public order: OrderService) {
    this.formOrder = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required)
    })
  }

  ngOnInit(): void {
  }

  submitOrder() {// example
    console.log();

    this.order.saveOrder(this.formOrder.value).subscribe(data => {
      this.order.clear();
      this.orderSent = true;
      this.submitted = false;

    })
    // console.log(JSON.stringify());

  }

  // }

}
