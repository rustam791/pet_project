import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { Order } from '../interface/order';
import { OrderService } from '../services/order.service';

@Injectable({
  providedIn: 'root'
})
export class OrderResolver implements Resolve<Order[]> {
  constructor(private orders: OrderService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Order[]> {
    return this.orders.getOrders();
  }
}
