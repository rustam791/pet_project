import { Injectable } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LoginApiService } from './login-api.service';

@Injectable({
  providedIn: 'root'
})
export class AsyncValidatorService {

  static LoginExist(api: LoginApiService, check_on_exist: boolean = true): AsyncValidatorFn {
    return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
      if (check_on_exist) {
        return api.getLoginState(control.value).pipe(map(
          (data: string) => {
            return data === 'valid' ? { 'Exist': true } : null;
          }
        ));
      } else {
        return api.getLoginState(control.value).pipe(map(
          (data: string) => {
            return data === 'invalid' ? { 'Exist': false } : null;
          }
        ))
      }
    };
  }
}
