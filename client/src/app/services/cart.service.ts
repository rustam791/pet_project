import { Injectable } from '@angular/core';
import { Product } from '../interface/product';
// import { CartLine } from '../interfaces/cart-line';

@Injectable({
  providedIn:"root"
})
export class CartService {

  public lines: CartLine[] = [];
  public itemCount: number = 0;
  public cartPrice: number = 0;
  constructor() { }

  addLine(product: Product, quantity: number = 1): void {
    const LINE: CartLine = <CartLine>this.lines.find(line => line.product.id === product.id);
    if (LINE) {
      LINE.quantity = quantity;
    } else {
      this.lines.push(new CartLine(product, quantity));
    }
    this.recalculate();
  }


  updateQuantity(product: Product, quantity: Event) {
    const LINE: CartLine = <CartLine>this.lines.find(line => line.product.id === product.id);
    if (LINE) {
      LINE.quantity = +(<HTMLInputElement>quantity.target).value;
    }
    this.recalculate();
  }

  removeLines(id: number) {
    this.lines = this.lines.filter(line => line.product.id !== id);
    this.recalculate();
  }

  clear() {
    this.lines = [];
    this.itemCount = 0;
    this.cartPrice = 0;
  }


  private recalculate() {
    this.itemCount = 0;
    this.cartPrice = 0;
    this.lines.map(line => {
      this.itemCount += line.quantity;
      this.cartPrice += (line.quantity * +line.product.price);
    });
  }


}


class CartLine {
  constructor(
    public product: Product,
    public quantity: number
  ) { }
  
  get lineTotal() {
    return this.quantity * +this.product.price;
  }
}