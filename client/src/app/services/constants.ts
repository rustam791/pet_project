export const API_URL: string = 'http://localhost:3500/';
export const PRODUCTS_PER_PAGE: number = 4;
export const STATE_ORDERS: string[] = [
    'новый',
    'создан',
    'в обработке',
    'отправлен',
    'доставлен',
    'отменен'
]