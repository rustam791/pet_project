import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from './constants';

@Injectable({
  providedIn: 'root'
})
export class LoginApiService {

  constructor(private http: HttpClient) { }

  getLoginState(login: string): Observable<string> {
    return this.http.get(`${API_URL}check-login`,
      {
        responseType: 'text',
        params: new HttpParams({
          fromObject: {
            login
          }
        })
      });
  }
}
