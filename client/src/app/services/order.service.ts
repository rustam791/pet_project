import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from '../interface/order';
import { CartService } from './cart.service';
import { API_URL } from './constants';

@Injectable({
  providedIn: 'root'
})
export class OrderService {


  public name!: string | null;
  public address!: string | null;
  public city!: string | null;
  public state!: string | null;
  public zip!: string | null;
  public country!: string | null;
  public shipped: boolean = false;

  constructor(public cart: CartService, private http: HttpClient) { }


  clear() {
    this.name = this.address = this.city = null;
    this.state = this.zip = this.country = null;
    this.shipped = false;
    this.cart.clear();
  }
  saveOrder(order: Object): Observable<Object> {
    console.log(JSON.stringify(order));
    return this.http.post(`${API_URL}order`, Object.assign(order, this.cart))
  }

  getOrders(): Observable<Order[]> {
    return this.http.get<Order[]>(`${API_URL}order/`);
  }
  deleteOrder(id: number): Observable<Order> {
    return this.http.delete<Order>(`${API_URL}order/${id}`); //fix returned id
  }
  updateOrder(order: Order): Observable<Order> {
    return this.http.put<Order>(`${API_URL}order/${order.id}`, order);
  }
}
