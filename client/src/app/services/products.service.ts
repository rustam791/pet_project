import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from '../interface/order';
import { Product } from '../interface/product';
import { API_URL } from './constants';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private products: Product[] = [];
  private categoris: string[] = [];
  constructor(
    public http: HttpClient
  ) {
    this.getAllProducts().subscribe(_product => {
      this.products = _product;
      this.categoris = ([...new Set(_product.map(p => p.category))]).sort();
    })

  }

  getAllProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(`${API_URL}products`)
  }

  getProducts(category: string | undefined = undefined): Product[] {
    return this.products
      .filter(p => category === undefined || category === p.category);
  }

  getProduct(id: number): Product | undefined {
    return this.products.find(p => p.id === id);
  }


  getCategories(): string[] {
    return this.categoris;
  }




  saveProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(`${API_URL}products`, product);
  }
  deleteProduct(id: number): Observable<Product> {
    return this.http.delete<Product>(`${API_URL}products/${id}`); //fix returned id
  }
  updateProduct(product: Product): Observable<Product> {
    return this.http.put<Product>(`${API_URL}products/${product.id}`, product);
  }

  


}
