import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Product } from '../interface/product';
import { CartService } from '../services/cart.service';
import { PRODUCTS_PER_PAGE } from '../services/constants';
import { ProductsService } from '../services/products.service';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {

  public selectedCategory: string | undefined;
  selectedPage: number = 1;
  productPerPage: number = 4;

  constructor(private productData: ProductsService, private cart: CartService) { }

  ngOnInit(): void { }


  get products(): Product[] {
    const PAGE_INDEX = (this.selectedPage - 1) * PRODUCTS_PER_PAGE;
    return this.productData.getProducts(this.selectedCategory)
      .slice(PAGE_INDEX, PAGE_INDEX + this.productPerPage);
  }

  getKey(i: number, product: Product): number {
    return product.id
  }

  get categories(): string[] {
    return this.productData.getCategories();
  }

  changeCategory(newCategory?: string | undefined) {
    this.selectedCategory = newCategory;
  }

  changePage(newPage: number) {
    this.selectedPage = newPage;
  }

  changePageSize(newSize: Event) {
    this.productPerPage = +(<HTMLSelectElement>newSize.target).value;
    this.changePage(1);
  }

  get pageNumbers(): number[] {
    return Array(Math.ceil(this.productData.getProducts(this.selectedCategory).length / this.productPerPage))
      .fill(0).map((x, i) => i + 1)
  }

  get pageCount(): number[] {
    return Array(
      Math.ceil(
        this.productData
          .getProducts(this.selectedCategory).length / this.productPerPage));
  }

  getProductCount():number {
    console.log("getProductCount invoked");
    return 1;
  }

  addProductToCart(product: Product) {
    this.cart.addLine(product);
  }

}
