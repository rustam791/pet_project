import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StoreRoutingModule } from './store-routing.module';
import { StoreComponent } from './store.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PaginationDirective } from '../directives/pagination.directive';
import { TestIfDirective } from '../directives/test-if.directive';
import { TestForDirective } from '../directives/test-for.directive';



@NgModule({
  declarations: [
    StoreComponent,
    PaginationDirective,
    TestIfDirective,
    TestForDirective
  ],
  imports: [
    CommonModule,
    StoreRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
  // exports: [StoreComponent],

})
export class StoreModule { }
